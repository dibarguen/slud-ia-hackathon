<?php
/**
 * Global config for DB Conexion
 */
class Config
{
    const DB_NAME     = '';
    const DB_USERNAME = '';
    const DB_PASSWORD = '';
    const DB_HOST     = '';
    const DB_CHAR     = '';
    const DB_OPT      = '';
}
